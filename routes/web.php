<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */

$router->get('/', function ()  {
    return 'hello';
});

$router->group(['prefix' => 'api/v1/collection/'], function ($router) {
    $router->get('/', 'CollectionsController@index'); //get all the routes	
    $router->post('/', 'CollectionsController@store'); //store single route
    $router->get('/{id}/', 'CollectionsController@show'); //get single route
    $router->put('/{id}/', 'CollectionsController@update'); //update single route
});

$router->group(['prefix' => 'api/v1/order/'], function ($router) {
    $router->post('create', 'OrdersController@create'); //get all the routes	
});


