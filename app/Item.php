<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model {

    protected $fillable = [];
    protected $dates = [];
    public static $rules = [
            // Validation rules
    ];

    // Relationships
    public function order() {
        return $this->belongsTo('App\Order');
    }

    public function collection() {
        return $this->belongsTo('App\Collection');
    }

}
