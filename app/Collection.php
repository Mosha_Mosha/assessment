<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model {

    protected $fillable = ['name'];
    protected $dates = [];
    public static $rules = [
            // Validation rules
    ];

    // Relationships
    public function items() {
        return $this->hasMany('App\Item');
    }

}
