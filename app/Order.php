<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {

    protected $fillable = [];
    protected $dates = [];
    public static $rules = [
            // Validation rules
    ];

    // Relationships
    public function items() {
        return $this->hasMany('App\Item');
    }

}
