<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Item;
use App\Collection;

class OrdersController extends Controller {

    /**
     * test post json.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function testpost() {
//die('check');

        $order_json = '
            {"order": {
                "order_id": 51275,
                "email": "test@email.com",
                "total_amount_net": "1890.00",
                "shipping_costs": "29.00",
                "payment_method": "VISA",
                "items": [
                  {
                    "name": "Item1",
                    "qnt": 1,
                    "value": 1100,
                    "category": "Fashion",
                    "subcategory": "Jacket",
                    "tags": [
                      "porsche",
                      "design"
                    ],
                    "collection_id": 12
                  },
                  {
                    "name": "Item2",
                    "qnt": 1,
                    "value": 790,
                    "category": "Watches",
                    "subcategory": "sport",
                    "tags": [
                      "watch",
                      "porsche",
                      "electronics"
                    ],
                    "collection_id": 7
                  }
                  ]
                }}';

        $url = 'localhost:8000/order/create';

        $ch = curl_init($url);

        $jsonData = array(
            'username' => 'MyUsername',
            'password' => 'MyPassword'
        );

        $jsonDataEncoded = json_encode($jsonData);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);

        return response()->json(['status' => 'success']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {
        $post = json_decode($request->parameters, true);

        if (Order::where('id', '=', $post['order']['order_id'])->exists()) {
            return response()->json(['id' => ['this order id is already exist']]);
        }
        $order = new Order();
        $order->id = $post['order']['order_id'];
        $order->email = $post['order']['email'];
        $order->total_amount_net = $post['order']['total_amount_net'];
        $order->shipping_costs = $post['order']['shipping_costs'];
        $order->payment_method = $post['order']['payment_method'];
        $order->save();
        // saving items
        $order_has_discount_value = TRUE;
        foreach ($post['order']['items'] as $item) {
            $item_data = new Item();
            $item_data['order_id'] = $order->id;
            $item_data->name = $item['name'];
            $item_data->qnt = $item['qnt'];
            $item_data->value = $item['value'];
            $item_data->category = $item['category'];
            $item_data->subcategory = $item['subcategory'];
            $item_data->collection_id = $item['collection_id'];
            $item_data->tags = implode(',', $item['tags']);
            $item_data->save();
            $item_data->order()->associate($order);
            // check if collection give discount

            if (!Collection::where('id', '=', $item['collection_id'])->exists()) {
                $order->delete();
                return response()->json(['collection_id' => ["this collection id $item[collection_id] is not exist in collections list, Please add it first"]]);
            }

            $collection = Collection::find($item['collection_id']);
            if (!$collection->has_discount) {
                $order_has_discount_value = FALSE;
            }
        }


        // if discount will applied
        if ($order_has_discount_value) {
            // get discount count
            $str = file_get_contents('https://developer.github.com/v3/#http-redirects');
            $page_words = array_count_values(str_word_count(strip_tags(strtolower($str)), 1));
            $discount_value = $page_words['status'];

            // check if exceeds the limit
            if ($discount_value / $order->total_amount_net > 0.25) {
                $discount_value = $order->total_amount_net * 0.25; // as max value
            }

            $result['status'] = 'success';
            $result['message'] = 'order is created';
            $result['discount_value'] = $discount_value;
        } else {
            $result['discount_value'] = 0;
        }
        $result['order'] = $post['order'];

        return response()->json($result);
    }

}
