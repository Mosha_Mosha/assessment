<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Collection;

class CollectionsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     */
    public function index() {
        //
        $collections = Collection::all();
        return response()->json($collections);
    }

    /**
     * Get the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $collection = Collection::where('id', $id)->get();
        if (!empty($collection['items'])) {
            return response()->json($collection);
        } else {
            return response()->json(['status' => 'fail']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
        $this->validate($request, [
            'name' => 'required',
            'has_discount' => 'boolean',
        ]);

        $collection = new Collection();
        $collection->name = $request->name;
        $collection->has_discount = $request->has_discount;

        $collection->save();
        
        return response()->json(['status' => 'success']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
        $this->validate($request, [
            'name' => 'required',
            'has_discount' => 'boolean',
        ]);

        $collection = Collection::find($id);
        $collection->name = $request->name;
        $collection->has_discount = $request->has_discount;

        $collection->save();
        return response()->json(['status' => 'success']);
    }

}
